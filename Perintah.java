
import java.awt.Dimension;
/**
 * 
 * Perintah.java
 * <br><br>
 * Class {@code Perintah} merepresentasikan perintah-perintah umum yang 
 * dapat diberikan kepada kura-kura. Termasuk dalam class ini adalah
 * proses untuk membaca input (saat ini baru melalui satu baris perintah)
 * dan memanggil method yang berkesesuaian.
 * Dalam kelas ini juga disediakan method-method yang merupakan kumpulan-kumpulan
 * perintah berurutan yang dapat diterima oleh kurakura dan menghasilkan gambar 
 * tertentu. 
 * <br><br>
 * Tugas anda pada file ini: <br>
 * - Lengkapi javadoc comment pada tiap deklarasi method.<br>
 * - Lengkapi inline comment untuk tiap baris perintah yang penting.<br>
 * - Perbaiki method {@code lakukan} agar tidak menimbulkan error bila input salah<br>
 * - Buat (1) perintah {@code mundur <x>}" agar kura-kura bisa mundur sebanyak x pixel satuan.
 * - Buat (2) perintah {@code hadap kanan} dan {@code hadap kiri} yang akan membuat kura-kura 
 *   menghadap ke kanan (rotasi 90) dan ke kiri (rotasi -90) 
 * - Dapatkah anda membuat (3) kura-kura mengenali perintah 
 *   {@code loop 10 perintah-perintah} <br>
 *   yang artinya melakukan perintah-perintah sebanyak 10 kali? <br>
 *   contoh: "loop 10 rotasi 30 maju 30" <br>
 *           yang artinya akan melakukan perintah "rotasi 30", dilanjutkan <br>
 *           perintah "maju 30", secara berulang-ulang sebanyak 10 kali<br>
 *   contoh: "loop 5 maju 20 hadap kanan maju 30" <br>
 *           yang artinya akan melakukan perintah "maju 20", dilanjutkan <br>
 *           "hadap kanan", kemudian perintah "maju 10", <br> 
 *           secara berulang-ulang sebanyak 5 kali<br>
 * 
 * @author Ade Azurat for DPBO 2008 @ Fasilkom UI
 * @author Ade Azurat for DDP2 2023 @ Fasilkom UI
 */
public class Perintah {
    Canvas canvas;
    Kurakura kurakuraku; 
    
    /** Creates a new instance of Perintah */
    public Perintah(Kurakura k, Canvas canvas) {
        kurakuraku = k;
        this.canvas = canvas;
    }

    /**
     * Method untuk menjalankan perintah-perintah dari user berdasarkan 
     * daftar perintah yang tersedia
     * @param inputPerintah
     * @return
     */
    public String lakukan(String inputPerintah){
        String[] in = inputPerintah.split(" ");
        // Variabel perintah di buat untuk index pertama input user agar mudah di apply pada tiap switch case
        String perintah = in[0].toLowerCase();
        switch(perintah){
        case "selesai":
            System.exit(0);
            break;
        case "reset":
            kurakuraku.reset();
            break;
        case "maju":
          try {
            kurakuraku.maju(Integer.parseInt(in[1]));
            break;
          } catch (Exception e) {
            canvas.repaint();
            return "Masukkan parameter untuk "+perintah;
          }
        case "mundur":
          try {
            kurakuraku.mundur(Integer.parseInt(in[1]));
            break;
          } catch (Exception e) {
            canvas.repaint();
            return "Masukkan parameter untuk "+perintah;
          }
        case "kanan":
          kurakuraku.rotasi(90);
          break;
        case "kiri":
          kurakuraku.rotasi(-90);
          break;
        case "rotasi":
          try {
            kurakuraku.rotasi(Integer.parseInt(in[1]));
            break;
          } catch (Exception e) {
            canvas.repaint();
            return "Masukkan parameter untuk "+perintah;
          }
        case "kotak":
          try {
            buatKotak(Integer.parseInt(in[1]));
            break;
          } catch (Exception e) {
            canvas.repaint();
            return "Masukkan parameter untuk "+perintah;
          }
        case "segitiga":
          try {
            buatSegitiga(Integer.parseInt(in[1]));
            break;
          } catch (Exception e) {
            canvas.repaint();
            return "Masukkan parameter untuk "+perintah;
          }
        case "segitigasiku":
          try {
            buatSikusiku(Integer.parseInt(in[1]),Integer.parseInt(in[2]));
            break;
          } catch (Exception e) {
            return "Masukkan parameter untuk "+perintah;
          }
        case "persegi":
          try {
            buatPersegi(Integer.parseInt(in[1]),Integer.parseInt(in[2]));
            break;
          } catch (Exception e) {
            canvas.repaint();
            return "Masukkan parameter untuk "+perintah;
          }
        case "pohon":
          buatPohon();
          break;
        case "sierpinski":
          try {
            sierpinski(Integer.parseInt(in[1]));
            break;
          } catch (Exception e) {
            canvas.repaint();
            return "Masukkan parameter level untuk "+perintah;
          }
        case "boxes":
          buatNestbox();
          break;
        case "kochcurve":
          try {
            kochCurve(Integer.parseInt(in[1]));
            break;
          } catch (Exception e) {
            canvas.repaint();
            return "Masukkan parameter level untuk "+perintah;
          }
        case "jejak":
          try {
            kurakuraku.setJejak(Boolean.parseBoolean(in[1]));
            break;
          } catch (Exception e) {
            canvas.repaint();
            return "Masukkan booleaan yang tepat. (true/false)";
          }
        case "pindah":
          try {
            kurakuraku.setPosition(new Dimension(Integer.parseInt(in[1]),Integer.parseInt(in[2])));
            break;
          } catch (Exception e) {
            canvas.repaint();
            return "Masukkan parameter untuk "+perintah;
          }
        default:
          canvas.repaint();
          return "Perintah tidak di pahami.";
        }
        canvas.repaint();    
        return "Perintah sudah dilaksanakan.";

    }
    /**
     * Method/perintah untuk membuat sebuah persegi
     * @param ukuran sebagai panjang tiap 
     *               sisi pada persegi
     */
    public void buatKotak(int ukuran ){        
        for (int i=0;i<4;i++){
            kurakuraku.maju(ukuran);
            kurakuraku.rotasi(90);
        }
    }
    /**
     * Method untuk membuat sebuag segitiga sama sisi
     * @param ukuran sebagai panjang setiap sisi
     *               pada segitiga
     */
    public void buatSegitiga(int ukuran){
        // setiap sudut pada segitiga sama sisi memiliki derajat 60
        for (int i=0; i<3;i++){
            kurakuraku.maju(ukuran);
            kurakuraku.rotasi(-120);//agar segitiga menghadap ke atas maka dimasukkan derajat -120
        }
    }
    /**
     * Method untuk membuat segitiga siku-siku
     * @param alas sebagai alas dan
     * @param tinggi sebagai tinggi dari segitiga tersebut
     */
    public void buatSikusiku(int alas, int tinggi){
        // Menghitung panjang sisi miring
        double hipo = Math.sqrt((alas * alas) + (tinggi * tinggi));
        // Membuat alas
        kurakuraku.rotasi(180);
        kurakuraku.maju(alas);
        kurakuraku.rotasi(90);
        // Membuat tinggi
        kurakuraku.maju(tinggi);
        kurakuraku.rotasi(180);

        // Kalkulasi untuk berapa derajat agar kura-kura rotasi untuk membuat hipotanusa
        kurakuraku.rotasi(-Math.toDegrees(Math.sin(alas/hipo)));
        kurakuraku.maju(hipo);
    }

    /**
     * Method untuk membuat persegi panjang
     * @param panjang sebagai panjang dan
     * @param lebar sebagai lebar dari persegi panjang tersebut
     */
    public void buatPersegi(int panjang, int lebar){
        // setiap iterasi melakukan satu sisi panjang dan satu sisi lebar
        for (int i=0; i<2;i++){
            kurakuraku.maju(panjang);
            kurakuraku.rotasi(90);
            kurakuraku.maju(lebar);
            kurakuraku.rotasi(90);
        }
    }
    /**
     * Method untuk membuat pohon dimana setiap ujung garis akan 
     * di buat 3 ranting bercabang
     * 
     */
    public void buatPohon(){        
        kurakuraku.setJejak(false);
        kurakuraku.reset();
        kurakuraku.rotasi(90);
        kurakuraku.maju(100);
        kurakuraku.rotasi(180);
        buatPohon(3,50);        
        kurakuraku.reset();
    }
    private void buatPohon(int ukuran, int tinggi){
        if (ukuran>0){
          kurakuraku.setJejak(true);
          kurakuraku.maju(tinggi);                        
          if(ukuran==1){
            buatKotak(8);
          }
            kurakuraku.rotasi(-45);
            Dimension posAwal = kurakuraku.getPosition();
            double arah = kurakuraku.getArah();
            double sudut = arah;
            for(int i=0;i<3;i++){  
                buatPohon(ukuran-1,(int)(tinggi/1.5));
                kurakuraku.setJejak(false);
                kurakuraku.setPosition(posAwal);
                kurakuraku.setArah(arah);                
                sudut+=45;
                kurakuraku.rotasi(sudut);  
            }     
        }
        kurakuraku.reset();
    }
    
    public void buatNestbox(){
      kurakuraku.setJejak(false);
      kurakuraku.rotasi(180);
      kurakuraku.maju(50);
      kurakuraku.rotasi(90);
      kurakuraku.maju(50);
      kurakuraku.rotasi(90);;
      buatNestbox(120);
    }
 
    private void buatNestbox(int ukuran){
      if(ukuran >= 0){
        buatKotak(ukuran);
        kurakuraku.setJejak(false);
        kurakuraku.maju(10);
        kurakuraku.rotasi(90);
        kurakuraku.maju(10);
        kurakuraku.rotasi(-90);
        kurakuraku.setJejak(true);
        buatNestbox(ukuran-20);
      }
    }
    /**
     * 
     * Menerima @param batas sebagai berapa level/lapis 
     * pada setiap segitiga dengan menghabiskan iterasi 
     * pertama dari segitiga kiri bawah, kanan bawah, 
     * lalu segitiga pada bagian atas
     */
    public void sierpinski(int batas){
      kurakuraku.setJejak(false);
      kurakuraku.rotasi(180);
      kurakuraku.maju(180);
      kurakuraku.rotasi(-90);
      kurakuraku.maju(80);
      kurakuraku.rotasi(-90);
      kurakuraku.setJejak(true);
      sierTriangle(150, batas);

    }


    private void sierTriangle(double ukuran, int limit){
      kurakuraku.maju(ukuran/2);
      Dimension pos2 = kurakuraku.getPosition();
      double arah = kurakuraku.getArah();
      kurakuraku.maju(ukuran/2);
      kurakuraku.rotasi(-120);
      kurakuraku.maju(ukuran);
      kurakuraku.rotasi(-120);
      kurakuraku.maju(ukuran/2);
      Dimension pos3 = kurakuraku.getPosition();
      kurakuraku.maju(ukuran/2);
      kurakuraku.rotasi(-120);

      if (limit > 0){
        sierTriangle(ukuran/2, limit-1);
        kurakuraku.setPosition(pos2);
        sierTriangle(ukuran/2, limit-1);
        kurakuraku.setPosition(pos3);
        kurakuraku.setArah(arah);
        sierTriangle(ukuran/2, limit-1);

      }
    }
    
    public void kochCurve(int level){
      kurakuraku.setJejak(false);
      kurakuraku.mundur(180);
      kurakuraku.setJejak(true);
      kochCurve(300, level);
    }

    private void kochCurve(double panjang, int level){
      if(level ==0){
        kurakuraku.maju(panjang);
      }
      else{panjang = panjang/3;
      kochCurve(panjang, level-1);
      kurakuraku.rotasi(-60);
      kochCurve(panjang, level-1);
      kurakuraku.rotasi(120);
      kochCurve(panjang, level-1);
      kurakuraku.rotasi(-60);
      kochCurve(panjang, level-1);
      }
    }
}
